# V Pipeline

[![pipeline status](https://gitlab.com/zacharymeyer/vlang/badges/main/pipeline.svg)](https://gitlab.com/zacharymeyer/vlang/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Build, test, and lint vlang projects.

- Enforces code style with [vfmt](https://vlang.io/)
- Unit testing with [vtest](https://github.com/vlang/v/blob/master/doc/docs.md#testing)
